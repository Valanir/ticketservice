package GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static javax.swing.JOptionPane.showMessageDialog;

public class LoginMenu extends JFrame{

    public static void main(String[] args) {
        GUI.LoginMenu app = new GUI.LoginMenu();
        app.setVisible(true);
    }
    private JLabel loginLabel = new JLabel("Login: ");
    private JLabel passLabel = new JLabel("Password: ");
    private JTextField loginField = new JTextField("" , 5);
    private JPasswordField passwordField = new JPasswordField("", 5);
    private JButton buttonLogin = new JButton("Login");
    private JButton buttonRegister = new JButton("Register");
    private JTextArea textArea = new JTextArea("TextArea");


    public LoginMenu(){
        super("Test program");
        this.setSize(250, 100);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int width = 250;
        int height = 100;
        this.setBounds((dim.width- width)/2, (dim.height-height)/2 , width, height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3,2));
        container.add(loginLabel);
        container.add(loginField);
        container.add(passLabel);
        container.add(passwordField);
        container.add(buttonRegister);
        container.add(buttonLogin);

        buttonLogin.addActionListener(new buttonLoginListener());
        buttonRegister.addActionListener(new buttonRegisterListener());
    }
    class buttonLoginListener implements ActionListener {
        public void actionPerformed (ActionEvent e){
            showMessageDialog(null, "Login");
        }
    }

    class buttonRegisterListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            GUI.RegisterPannel registerPannel = new RegisterPannel();
            registerPannel.setVisible(true);
        }
    }
}
