package GUI;

import UserDAO.UserController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicBoolean;

import static javax.swing.JOptionPane.showMessageDialog;

public class RegisterPannel extends JFrame {
    static UserController users = new UserController();
    private JLabel loginLable = new JLabel("Login: ");
    private JTextField loginField = new JTextField("", 5);
    private JLabel passLable = new JLabel("Password: ");
    private JLabel confirmPassLable = new JLabel("Confirm password: ");
    private JPanel loginPanel = new JPanel();
    private JPanel passPanel = new JPanel();
    private JPanel confirmPassPanel = new JPanel();
    private JPanel infoPannel = new JPanel();
    private JPanel buttonPanel = new JPanel();
    private JTextField passField = new JTextField("", 5);
    private JTextField confirmPassField = new JTextField("", 5);
    private JLabel statusLable = new JLabel("");
    private JButton registeButton = new JButton("Register");

    public RegisterPannel(){
        super("Register");
        this.setSize(250, 100);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int width = 250;
        int height = 150;
        this.setBounds((dim.width- width)/2, (dim.height-height)/2 , width, height);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);



        loginPanel.setLayout(new GridLayout(1,2));
        loginPanel.add(loginLable);
        loginPanel.add(loginField);
        loginPanel.setSize(250, 50);

        passPanel.setLayout(new GridLayout(1,2));
        passPanel.add(passLable);
        passPanel.add(passField);
        passPanel.setSize(250, 50);

        confirmPassPanel.setLayout(new GridLayout(1,2));
        confirmPassPanel.add(confirmPassLable);
        confirmPassPanel.add(confirmPassField);
        confirmPassPanel.setSize(250, 50);

        infoPannel.setLayout(new BorderLayout());
        statusLable.setHorizontalAlignment(JLabel.CENTER);
        statusLable.setVerticalAlignment(JLabel.CENTER);
        infoPannel.add(statusLable, BorderLayout.CENTER);

        buttonPanel.setLayout(new GridLayout(1,2));
        buttonPanel.add(registeButton);



        Container window = this.getContentPane();
        window.setLayout(new GridLayout(5,1));
        window.add(loginPanel);
        window.add(passPanel);
        window.add(confirmPassPanel);
        window.add(infoPannel);
        window.add(buttonPanel);

        this.setVisible(true);
        this.setResizable(false);

        registeButton.addActionListener(new registerButtomListener());



    }

    class registerButtomListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            AtomicBoolean uniqLogin = new AtomicBoolean(true);
            boolean samePass = false;

            if (passField.getText().isEmpty() || confirmPassField.getText().isEmpty()
                    || loginField.getText().isEmpty()) {
                statusLable.setText("Write all parametrs");
                statusLable.setForeground(Color.RED);
            }

            if (passField.getText().equals(confirmPassField.getText())) samePass = true;
            else {statusLable.setText("Passwords is not equals");
                  statusLable.setForeground(Color.RED);}

            users.loadUserData();

            users.getAllUsers().stream().forEach(u -> {
                if (u.getLogin().equalsIgnoreCase(loginField.getText())){
                    uniqLogin.set(false);
                    statusLable.setText("login is busy");
                    statusLable.setForeground(Color.RED);
                }
            });

            if (uniqLogin.get() == true && samePass == true){
                users.createUser(loginField.getText(), passField.getText());
                statusLable.setText("User was register");
                statusLable.setForeground(Color.GREEN);
                showMessageDialog(null, "User " + loginField.getText() +
                        " create successfully");
                users.getAllUsers().stream().forEach(u -> System.out.println(u));

            }




            // действие кнопки
            // если логин свободный
            // проверка пароля
        }
    }
}
