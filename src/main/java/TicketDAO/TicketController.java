package TicketDAO;

import FlightDAO.Flight;
import UserDAO.User;

public class TicketController {
    public static TicketService ticketService;

    public TicketController(){
        this.ticketService = new TicketService();
    }

    public void saveTicketData(){
        ticketService.saveTicketData();
    }

    public void loadTicketData(){
        ticketService.loadTicketData();
    }

    public void createTicket(Flight flight, User owner, String passenger, Flight nextFlight){
        ticketService.createTicket(flight, owner, passenger, nextFlight);
    }

    public void createTicket(Flight flight, User owner, String passenger){
        ticketService.createTicket(flight, owner, passenger);
    }

    public void removeTicket(Flight flight, User owner){
        ticketService.removeTicket(flight, owner);
    }

}
