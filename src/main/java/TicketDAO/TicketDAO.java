package TicketDAO;

import FlightDAO.Flight;
import UserDAO.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TicketDAO {
    private List<Ticket> tickets;

    public TicketDAO(){
        this.tickets = new ArrayList<Ticket>();
    }

    public void saveTicketData(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("ticket.dat"))){
            oos.writeInt(tickets.size());
            for (Ticket ticket: tickets) {
                oos.writeObject(ticket);
            }
            oos.flush();
            System.out.println("File save successfully");
        }catch (IOException e){
            System.out.println("Error");
        }
    }

    public void loadTicketData(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("ticket.dat"))){
            int count = ois.readInt();
            for (int i = 0; i < count; i++){
                Ticket ticket = (Ticket) ois.readObject();
                tickets.add(ticket);
            }
            System.out.println("Tickets was load successfully");

        }catch (IOException e){
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    public void createTicket(Flight flight, User owner, String passenger, Flight nextFlight){
        tickets.add(new Ticket(flight, owner, passenger, nextFlight));
    }

    public void createTicket(Flight flight, User owner, String passenger) {
        tickets.add(new Ticket(flight, owner, passenger));
    }

    public void removeTicket(Flight flight, User owner){
        for(Ticket ticket: tickets){
            if(ticket.getFlight().equals(flight) &&
                    ticket.getOwner().equals(owner)) ticket = null;
        }
    }
}
