package TicketDAO;

import FlightDAO.Flight;
import UserDAO.User;

public class Ticket {
    private Flight flight;
    private User owner;
    private String passenger;
    private Flight nextFlight;

    public Ticket (Flight flight, User owner, String passenger, Flight nextFlight){
        this.flight = flight;
        this.owner = owner;
        this.passenger = passenger;
        this.nextFlight = nextFlight;
    }

    public Ticket (Flight flight, User owner, String passenger){
        this(flight, owner, passenger, null);
    }



    @Override
    public String toString() {
        return "Ticket{" +
                "flight=" + flight +
                ", owner=" + owner +
                ", passenger='" + passenger +
                ", nextFlight=" + nextFlight.getNameFlight() +
                '}';
    }

    public Flight getFlight() {
        return flight;
    }

    public User getOwner() {
        return owner;
    }
}
