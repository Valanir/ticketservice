package TicketDAO;

import FlightDAO.Flight;
import UserDAO.User;

public class TicketService {
    public static TicketDAO ticketDAO;

    TicketService(){
        this.ticketDAO = new TicketDAO();
    }

    public void saveTicketData(){
        ticketDAO.saveTicketData();
    }

    public void loadTicketData(){
        ticketDAO.loadTicketData();
    }

    public void createTicket(Flight flight, User owner, String passenger, Flight nextFlight){
        ticketDAO.createTicket(flight, owner, passenger, nextFlight);
    }

    public void createTicket(Flight flight, User owner, String passenger){
        ticketDAO.createTicket(flight, owner, passenger);
    }

    public void removeTicket(Flight flight, User owner){
        ticketDAO.removeTicket(flight, owner);
    }
}
