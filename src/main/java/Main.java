import FlightDAO.FlightController;
import FlightDAO.FlightService;
import TicketDAO.TicketController;
import UserDAO.User;
import UserDAO.UserController;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class Main{
    static UserController users = new UserController();
    static FlightController flights = new FlightController();
    static TicketController tickets = new TicketController();
    public static void main(String[] args) {
        GUI.LoginMenu app = new GUI.LoginMenu();
        app.setVisible(true);



        users.createUser("test", "1234");
        users.createUser("reet", "ddfsdf");
        users.getUserById(0).changePassword("55555");
        users.getUserByLogin("test").changePassword("4565");
        flights.getFlightByID(5);
        saveUserData();




    }
    public static void saveUserData(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"))){
            oos.writeInt(users.getAllUsers().size());
            System.out.println(users.getAllUsers().size());
            for (User user: users.getAllUsers()) {
                oos.writeObject(user);
            }
            //System.out.println("File save successfully");
        }catch (IOException e){
            System.out.println("Error save user");
        }

    }
}