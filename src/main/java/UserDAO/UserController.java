package UserDAO;

import TicketDAO.Ticket;

import java.util.List;

public class UserController {
    private static UserService userService;

    public UserController(){
        this.userService = new UserService();
    }

    public void loadUserData(){
        userService.loadUserData();
    }

    public void saveUserData(){
        userService.saveUserData();
    }

    public void createUser(String login, String password){
        userService.createUser(login, password);
    }

    public User getUserById(int id){
        return userService.getUserById(id);
    }

    public User getUserByLogin(String login){
        return userService.getUserByLogin(login);
    }

    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }
    public void addTicket(String login, Ticket ticket){
        userService.addTicket(login, ticket);
    }

    public List<Ticket> getUserTickets(String login){
        return userService.getUserTickets(login);
    }

    public void removeTicket(String login, Ticket ticket){
        userService.removeTicket(login, ticket);
    }

    public String getPasswordByLogin(String login){
        return userService.getPasswordByLogin(login);
    }
}
