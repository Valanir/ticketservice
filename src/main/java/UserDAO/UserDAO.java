package UserDAO;

import TicketDAO.Ticket;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private List<User> users;

    public UserDAO(){
        this.users = new ArrayList<User>();
    }

    public void saveUserData(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"))){
            oos.writeInt(users.size());
            System.out.println(users.size());
            for (User user: users) {
                oos.writeObject(user);
            }
            //System.out.println("File save successfully");
        }catch (IOException e){
            System.out.println("Error save user");
        }

    }

    public void loadUserData(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("users.dat"))){
            int count = ois.readInt();
            for (int i = 0; i < count; i++){
                User user = (User) ois.readObject();
                users.add(user);
            }
            System.out.println("User was load successfully");

        }catch (IOException | ClassNotFoundException e){
        }

    }

    public void createUser(String login, String password){
        users.add(new User(login, password));
    }

    public List<User> getAllUser(){
        return users;
    }
    public User getUserById(int id){
        for(User user : users){
            if (user.id == id) return user;
        }
        return null;
    }

    public User getUserByLogin(String login){
        for (User user : users){
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public void addTicket(String login, Ticket ticket){
        getUserByLogin(login).addTicket(ticket);
    }

    public List<Ticket> getUserTickets(String login){
        return getUserByLogin(login).getTickets();
    }

    public void removeTicket(String login, Ticket ticket){
        getUserByLogin(login).getTickets().remove(ticket);
    }
    public String getPasswordByLogin(String login){
        return getUserByLogin(login).getPassword();
    }
}
