package UserDAO;

import TicketDAO.Ticket;

import java.util.List;

class UserService {
    private static UserDAO userDAO;

    UserService(){
        this.userDAO = new UserDAO();
    }

    public void loadUserData(){
        userDAO.loadUserData();
    }

    public void saveUserData(){
        userDAO.saveUserData();
    }

    public void createUser(String login, String password){
        userDAO.createUser(login, password);
    }

    public User getUserById(int id){
        return userDAO.getUserById(id);
    }

    public User getUserByLogin(String login){
        return userDAO.getUserByLogin(login);
    }

    public List<User> getAllUsers(){
        return userDAO.getAllUser();
    }

    public void addTicket(String login, Ticket ticket){
        userDAO.addTicket(login, ticket);
    }

    public List<Ticket> getUserTickets(String login){
        return userDAO.getUserTickets(login);
    }

    public void removeTicket(String login, Ticket ticket){
        userDAO.removeTicket(login, ticket);
    }

    public String getPasswordByLogin(String login){
        return userDAO.getPasswordByLogin(login);
    }

}
