package UserDAO;

import TicketDAO.Ticket;

import java.util.List;

public class User {
    String login;
    String password;
    int id;
    static int staticId = 0;
    List<Ticket> tickets;


    public User(String login, String password){
        this.login = login;
        this.password = password;
        this.id = staticId;
        this.tickets = null;
        staticId++;
    }

    public void changePassword(String password) {
        this.password = password;
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public String getLogin() {
        return login;
    }

    public int getId(){
        return id;
    }

    public String getPassword() {
        return password;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    @Override
    public String toString() {
        String ticketsList = "none";
        if (tickets != null) {
            ticketsList = "";
            for (Ticket ticket : tickets){
                ticketsList += ticket.getFlight().getNameFlight() + ", ";
            }
            ticketsList.substring(0, ticketsList.length() - 2);
        }
        return "User{" +
                "id= " + id +
                ", login='" + login +
                ", password='" + password +
                "', tickets=" + tickets +
                '}';
    }
}
