package Service;

import FlightDAO.NameOfCity;



public class showAvaibleCity {

    // метод отображает все континенты с городами
    public static void showAvaibleCity(){
        System.out.println("Європа: ");
        sortContinent("Європа");
        System.out.println("Азія: ");
        sortContinent("Азія");
        System.out.println("Північна Америка: ");
        sortContinent("Північна Америка");
    }

    // метод сортирует города по континентам
    public static void sortContinent(String continent){
        System.out.print("\t");
        for (NameOfCity city : NameOfCity.values()){
            switch (continent){
                case "Європа":
                    if (city.getContinent().equals("Європа")) System.out.print(city.getName() + ", ");
                    break;
                case "Азія":
                    if (city.getContinent().equals("Азія")) System.out.print(city.getName() + ", ");
                    break;
                case "Північна Америка":
                    if (city.getContinent().equals("Північна Америка")) System.out.print(city.getName() + ", ");
                    break;
            }
        }
        System.out.print("\b".repeat(2));
        System.out.println();
    }
}
