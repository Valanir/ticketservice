package Service;

import FlightDAO.Flight;
import FlightDAO.FlightController;
import FlightDAO.NameOfCity;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


public class FindTickets {
    static FlightController flightController = new FlightController();  // потом надо поменять сылку на наш общий класс


    // Метод ищет рейсы которые идут с Киева и в нужный город.
    // Возвращает List<Flight сначала прямые, потом те что с пересадкой
    public static List<Flight> findTickets(NameOfCity departTown,NameOfCity arrivalTown, int freeSeats){
        int timeTransfer = 12; // время для пересадки
        List<Flight> flights = new ArrayList<>();


        if (departTown != null && arrivalTown != null) {

            //поиск прямых рейсов
            flightController.getAllFlights().stream()
                    .filter(f -> ((f.getDeparture().equals(departTown)))
                            && (f.getArrival().getName().equalsIgnoreCase(arrivalTown.getName())
                            && (f.getFreeSeats() >= freeSeats)))
                    .forEach(f -> flights.add(f));

            //поиск рейсов с пересадкой
            List<Flight> search = flightController.getAllFlights();
            for(int i = 0; i < search.size() - 1; i++){
                for (int j = 1 + i ; j < search.size() ; j++){
                    if((search.get(i).getDeparture().equals(departTown))
                            && (search.get(i).getArrival().equals(search.get(j).getDeparture()))
                            && (search.get(j).getArrival().getName().equalsIgnoreCase(arrivalTown.getName()))
                            && (ChronoUnit.HOURS.between(search.get(i).getTimeArrival(), search.get(j).getTimeDeparture()) < timeTransfer &&
                                    ChronoUnit.HOURS.between(search.get(i).getTimeArrival(), search.get(j).getTimeDeparture()) > 0)){
                        flights.add(search.get(i));
                        flights.add(search.get(j));
                    }
                }
            }

        } else System.out.println("Напишіть правильне місто");
        return flights;
    }

}
