package Service;

import FlightDAO.Flight;
import FlightDAO.FlightController;
import FlightDAO.NameOfCity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class ShowFlightsFromKuiv {
    static FlightController flightController = new FlightController(); // потом надо поменять сылку на наш общий класс

    // метод генерирует общую таблицу с названием столбцов, фильтрует рейсы которые с Киева и в ближайшие 24 часа
    public static void showTablo(NameOfCity city){
        System.out.println("    ID        Departure        Arrival         Time   Gate");
        List<Flight> flights = new ArrayList<>();
        flightController.getAllFlights().stream().filter(f -> (f.getDeparture().equals(city)) &&
                        (ChronoUnit.HOURS.between(LocalDateTime.now(), f.getTimeDeparture()) < 24)).forEach(f -> flights.add(f));
        flights.stream().forEach(f -> {
            try {
                showLine(f);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    // метод принимает объекс рейса и красиво выводить его в консоль
    public static void showLine(Flight flight) throws InterruptedException{
        int iteration = 10;
        int timeDelay = 30;

        for (int i = 0; i <= iteration; i++){
            char[] genLine = new char[15];
            for (int j = 0; j < genLine.length; j++){
                genLine[j] = (char)((int) (Math.random() * (122 - 97)) + 97);
            }
            System.out.print("||");
            for (int k = 0; k < 5; k++){
                System.out.print(genLine[k]);
            }
            System.out.print(" | ");
            for (int k = 0; k < 15 - flight.getArrival().getName().length(); k++){
                System.out.print(" ");
            }
            for (int k = 0; k < flight.getArrival().getName().length(); k++){
                System.out.print(genLine[k]);
            }
            System.out.print(" | ");
            for (int k = 0; k < 15 - flight.getDeparture().getName().length(); k++){
                System.out.print(" ");
            }
            for (int k = 0; k < flight.getDeparture().getName().length(); k++){
                System.out.print(genLine[k]);
            }
            System.out.print(" | ");
            for (int k = 6; k < 11; k++){
                System.out.print(genLine[k]);
            }
            System.out.print(" | ");
            System.out.print(genLine[13]);
            System.out.print(genLine[14]);
            System.out.print(genLine[1]);
            System.out.print(" ||");
            Thread.sleep(timeDelay);
            System.out.print("\b".repeat(60));

        }
        System.out.printf("||%5s | %15s | %15s | %5s | %3s ||", flight.getId(), flight.getDeparture().getName(),flight.getArrival().getName(), flight.getTimeDeparture().format(DateTimeFormatter.ofPattern("hh:mm")), "A12");
        System.out.println();
    }
}
