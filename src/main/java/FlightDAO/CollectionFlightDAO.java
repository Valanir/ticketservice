package FlightDAO;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CollectionFlightDAO {
    private final List<Flight> flights;
    public CollectionFlightDAO(){
        this.flights = new ArrayList<>();
    }

    // метод возвращает все рейсы
    public List<Flight> getALLFlights() {
        return flights;
    }

    // метод возвращает рейс согласно позиции в списке
    public Flight getFlightByIndex(int index){
        return flights.get(index);
    }

    // метож возвращает рейс по id
    public Flight getFlightById(int id){
        Flight f = null;
        for(Flight flight : flights){
            if (flight.getId() == id) f = flight;
        }
        return f;
    }

    // метод сохраняет List<Flight> flights в файл flight.dat
    public void saveFlightData(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("flights.dat"))){
            oos.writeInt(flights.size());
            for (Flight flight: flights) {
                oos.writeObject(flight);
            }
            oos.flush();
            //logger.info("File date was save successful");
            System.out.println("Файл flights.dat збережено успішно");
        }catch (IOException e){
            System.out.println("Помилка збереження списку рейсів у файл flights.dat");
        }
    }

    // метод загружает в List<Flight> flights из файла flight.dat
    public void loadFlightData(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("flights.dat"))){
            int count = ois.readInt();
            for (int i = 0; i < count; i++){
                Flight flight = (Flight) ois.readObject();
                flights.add(flight);
            }
            System.out.println("База даних рейсів була успішно завантажена");

        }catch (IOException e){
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
