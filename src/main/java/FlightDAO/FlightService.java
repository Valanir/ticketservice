package FlightDAO;
import java.time.LocalDateTime;
import java.util.List;

public class FlightService {
    private static CollectionFlightDAO flightDAO;

    FlightService(){
        this.flightDAO = new CollectionFlightDAO();
    }

    // метод возвращает все рейсы
    public List<Flight> getAllFlights(){
        return flightDAO.getALLFlights();
    }

    // метод возвращает рейс согласно позиции в списке
    public Flight getFlightByIndex(int index){
        return flightDAO.getFlightByIndex(index);
    }

    // метод возвращает рейс согласно его id
    public Flight getFlightByID(int id){
        return flightDAO.getFlightById(id);
    }

    // метод сохраняет List<Flight> flights в файл flight.dat
    public void saveFlightData(){
        flightDAO.saveFlightData();
    }

    // метод загружает в List<Flight> flights из файла flight.dat
    public void loadFlightData(){
        flightDAO.loadFlightData();
    }
}
