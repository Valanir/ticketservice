package FlightDAO;
public enum NameOfCity {
    // список городов доступных для рейсов
    // при добавлении нового, города генератор базы автоматически подтянет их
    KYIV("Київ" , "Європа"),
    VINNITSIA("Вінниця", "Європа"),
    MYKOLAIV("Миколаїв", "Європа"),
    LVIV("Львів", "Європа"),
    IVANOFRANK("Івано-Франківськ", "Європа"),
    UZHOROD("Ужгород", "Європа"),
    ODESA("Одеса", "Європа"),
    KRIVIYRIG("Кривий Ріг", "Європа"),
    ATLANTA("Атланта", "Північна Америка"),
    BEIJING("Пекін", "Азія"),
    CHERNIVCI("Чернівці" , "Європа"),
    WARSAWA("Варшава", "Європа"),
    KATOWICE("Катовіце", "Європа"),
    PARIS("Париж", "Європа"),
    LONDON("Лондон", "Європа"),
    CHICAGO("Чикаго", "Північна Америка"),
    DUBAI("Дубаї", "Азія"),
    NEWYORK("Нью-Йорк", "Північна Америка"),
    ISTANBUL("Стамбул", "Європа"),
    SEOUL("Сеул", "Європа"),
    MADRID("Мадрид", "Європа"),
    LASVEGAS("Лас-Вегас", "Північна Америка"),
    MIAMI("Маямі", "Північна Америка"),
    BARCELONA("Барселона", "Європа"),
    TOKYO("Токіо", "Азія"),
    HONGONG("Гонконг", "Азія"),
    SHRILANKA("Шрі-Ланка", "Азія"),
    TAIBEI("Тайбей", "Азія"),
    PHENYAY("Пхенянь", "Азія"),
    ANTALYA("Анталія", "Європа");

    String name;
    String continent;
    NameOfCity(String name, String continent){
        this.name = name;
        this.continent = continent;
    }

    public String getName(){
        return name;
    }
    public String getContinent(){return continent;}
}
