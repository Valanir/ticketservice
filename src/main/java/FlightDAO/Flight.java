package FlightDAO;
import TicketDAO.Ticket;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Flight implements Serializable {
    static int staticId = 0;
    int id;   // id рейса
    NameOfCity departure; //пункт вылета
    NameOfCity arrival; //пункт назначения
    LocalDateTime timeDeparture; // дата и время вылета
    LocalDateTime timeArrival; // дата и время вылета
    List<Ticket> passenger = new ArrayList<>(); //список пассажиров
    int numOfSeats; //к-тво свободных мест
    String nameFlight;


    @Override
    public String toString() {
        return "Flight{" +
                "id='" + id + '\'' +
                ", departure='" + departure.getName() + '\'' +
                ", arrival='" + arrival.getName() + '\'' +
                ", timeDeparture=" + timeDeparture.format(DateTimeFormatter.ofPattern("hh:mm dd/MM/YYYY")) +
                ", timeArrival=" + timeArrival.format(DateTimeFormatter.ofPattern("hh:mm dd/MM/YYYY")) +
                ", freeSeats=" + numOfSeats +
                '}';
    }

    public Flight(NameOfCity departure, NameOfCity arrival, LocalDateTime timeDeparture, LocalDateTime timeArrival, int numOfSeats) {
        this.departure = departure;
        this.arrival = arrival;
        this.timeDeparture = timeDeparture;
        this.timeArrival = timeArrival;
        this.numOfSeats = numOfSeats;
        this.id = staticId;
        this.nameFlight = departure.toString() + " - " + arrival.toString();
        staticId++;
    }

    public int getId() {
        return id;
    }

    public String getNameFlight(){
        return nameFlight;
    }

    // возвращает город с которого вылет
    public NameOfCity getDeparture() {
        return departure;
    }
    // возвращает город куда летит
    public NameOfCity getArrival() {
        return arrival;
    }

    // возвращает дату и время вылета
    public LocalDateTime getTimeDeparture() {
        return timeDeparture;
    }

    // возвращает дату и время приземление
    public LocalDateTime getTimeArrival(){
        return timeArrival;
    }

    // врозвращает список пассажиров на текущем рейсе
    public List<Ticket> getPassenger() {
        return passenger;
    }

    // добавляет пассажира на рейс
    public void addPassenger(Ticket passenger) {
        if (getFreeSeats() > 0){
        this.passenger.add(passenger);
        }else System.out.println("Не має вільних місць");
    }

    // возвращает к-тво свободных мест в рейсе
    public int getFreeSeats() {
        return numOfSeats - passenger.size();
    }

}
