package FlightDAO;
import java.time.LocalDateTime;
import java.util.List;

public class FlightController {
    private final FlightService flightService;

    public FlightController(){
        this.flightService = new FlightService();
    }

    // метод возвращает все рейсы
    public List<Flight> getAllFlights(){
        return flightService.getAllFlights();
    }

    // метод возвращает рейс согласно позиции в списке
    public Flight getFlightByID(int id){
        return flightService.getFlightByID(id);
    }


    // метод возвращает рейс согласно его id
    public Flight getFlightByIndex(int index){
        if (index >= 0 && index < flightService.getAllFlights().size()) {
            return flightService.getFlightByIndex(index);
        } else return null;
    }

    // метод сохраняет List<Flight> flights в файл flight.dat
    public void saveFlightData(){
        //log
        flightService.saveFlightData();
    }

    // метод загружает в List<Flight> flights из файла flight.dat
    public void loadFlightData(){
        //log
        flightService.loadFlightData();
    }
}
